#pragma once
#include <vector>
#include <Snapshot.h>
#include <Events/Event.h>

namespace TCS_BattleTracker {
  struct Timeline : Serializable {
    std::vector<Snapshot> snapshots;
    std::vector<std::shared_ptr<Event>> events;

    void recordSnapshot(float time, const Transform& transform, VehicleStatus status) {
      if (this->snapshots.size() > 1) {
        auto& last = this->snapshots.at(this->snapshots.size() - 1);

        if (
          last.status == status
          && std::abs(last.transform.rotation - transform.rotation) < 5
          && (transform.position - last.transform.position).squaredMagnitude() < 1
          ) {
          last.time = time;
          last.transform = transform;
          return;
        }
      }

      Snapshot snapshot;
      snapshot.time = time;
      snapshot.transform = transform;
      snapshot.status = status;

      this->snapshots.push_back(snapshot);
    }

    void recordEvent(std::shared_ptr<Event> evt) {
      this->events.push_back(evt);
    }

    virtual void serialize(BinaryOutputStream& output) const override {
      output.write(static_cast<int32_t>(this->snapshots.size()));

      for (auto& snapshot : this->snapshots) {
        snapshot.serialize(output);
      }

      output.write(static_cast<int32_t>(this->events.size()));

      for (const auto& evt : this->events) {
        evt->serialize(output);
      }
    }
  };
}