#pragma once
#include <cstdint>
#include <string>

namespace TCS_BattleTracker {
  enum Side : int32_t {
    WEST = 0,
    EAST = 1,
    INDEPENDENT = 2,
    CIVILIAN = 3,
    UNKNOWN = 4
  };

  Side sideFromString(const std::string& sideString) noexcept;
}