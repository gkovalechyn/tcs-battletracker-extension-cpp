#pragma once
#include <cstdint>

namespace TCS_BattleTracker {
  enum VehicleStatus : int32_t {
    DEFAULT = 0,
    IN_VEHICLE = 1,
    UNCONSCIOUS = 2,
    DEAD = 3
  };
}