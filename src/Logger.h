#pragma once
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

namespace TCS_BattleTracker {
  static std::shared_ptr<spdlog::logger> FILE_LOGGER;

  void initializeLogger();
}