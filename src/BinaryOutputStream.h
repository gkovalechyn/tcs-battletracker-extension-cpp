#pragma once
#include <ostream>

/// <summary>
/// A simple binary stream class to write the recording file.
/// Only supports writing and in little-endian order.
/// </summary>
///
///
class BinaryOutputStream {
private:
  std::ostream* out;

  union FloatToBytes {
    float number;
    uint32_t asInt;
  };
public:
  BinaryOutputStream(std::ostream* outputStream) {
    this->out = outputStream;
  }

  void write(const char* ptr, size_t length) {
    this->out->write(ptr, length);
  }
  void write(char val) {
    this->out->put(val);
  }

  void write(int32_t val) {
    uint32_t newVal = *((uint32_t*)(&val));
    this->write(newVal);
  }

  void write(uint32_t val) {
    this->out->put((val >> 0) & 0xFF);
    this->out->put((val >> 8) & 0xFF);
    this->out->put((val >> 16) & 0xFF);
    this->out->put((val >> 24) & 0xFF);
  }

  void write(float val) {
    FloatToBytes tempUnion;
    tempUnion.number = val;

    this->write(tempUnion.asInt);
  }

};