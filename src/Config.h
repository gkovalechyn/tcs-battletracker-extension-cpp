#pragma once
#include <unordered_map>
#include <string>
namespace TCS_BattleTracker {
  class Config {
  private:
    std::unordered_map<std::string, std::string> config;

  public:
    Config();
    void loadFrom(const std::string& path);
    void saveTo(const std::string& path);

    std::string get(const std::string& key);
  };
}