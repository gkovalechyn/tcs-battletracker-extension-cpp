#pragma once
#include "StringUtils.h"
#include <spdlog/spdlog.h>
#include "Logger.h"

namespace TCS_BattleTracker {
  struct Vector3 : public Serializable {
    float x;
    float y;
    float z;

    Vector3(float x = 0, float y = 0, float z = 0) : x(x), y(y), z(z) {

    }

    static Vector3 fromArma3Array(const std::string& a3Array) {
      auto copy = StringUtils::trimCopy(a3Array);
      //remove "[" and "]"
      copy = copy.substr(1, copy.length() - 2);

      const auto parts = StringUtils::split(copy, ",");
      if (parts.size() < 2 || parts.size() > 3) {
        SPDLOG_ERROR("Wrong vector 3 format, received string: {}", a3Array);
        return Vector3();
      } else {
        auto result = Vector3();

        try {
          result.x = std::stof(parts[0]);
        } catch (const std::invalid_argument&) {
          SPDLOG_ERROR("Invalid number \"{}\"in vector {}", parts[0], a3Array);
        }

        try {
          result.y = std::stof(parts[1]);
        } catch (const std::invalid_argument&) {
          SPDLOG_ERROR("Invalid number \"{}\"in vector {}", parts[1], a3Array);
        }

        if (parts.size() > 2) {
          try {
            result.z = std::stof(parts[2]);
          } catch (const std::invalid_argument&) {
            SPDLOG_ERROR("Invalid number \"{}\"in vector {}", parts[2], a3Array);
          }
        }


        return std::move(result);
      }
    }

    Vector3& operator+=(const Vector3& rhs) {
      this->x += rhs.x;
      this->y += rhs.y;
      this->z += rhs.z;

      return *this;
    }

    Vector3& operator-=(const Vector3& rhs) {
      this->x -= rhs.x;
      this->y -= rhs.y;
      this->z -= rhs.z;

      return *this;
    }

    friend Vector3 operator+(Vector3 lhs, const Vector3& rhs) {
      lhs += rhs;

      return lhs;
    }

    friend Vector3 operator-(Vector3 lhs, const Vector3& rhs) {
      lhs -= rhs;

      return lhs;
    }

    float inline squaredMagnitude() {
      return this->x * this->x + this->y * this->y + this->z * this->z;
    }

    // Inherited via Serializable
    virtual void serialize(BinaryOutputStream& output) const override {
      output.write(this->x);
      output.write(this->y);
      output.write(this->z);
    };
  };
}