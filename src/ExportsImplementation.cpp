#include <string>
#include <iostream>
#include <sstream>

#include <Exports.h>
#include <Version.h>
#include <Logger.h>
#include <BattleTracker.h>
#include <Unused.h>

void RVExtensionVersion(char* output, int outputSize) {
  try {
    TCS_BattleTracker::initializeLogger();
    TCS_BattleTracker::BattleTracker::initialize();

    SPDLOG_INFO("BattleTracker loaded");

    ::memcpy(output, VERSION, std::min(sizeof(VERSION), static_cast<size_t>(outputSize)));
    
  } catch (std::exception& e) {
    std::cout << e.what() << std::endl;
    SPDLOG_CRITICAL(e.what());
  }
}

void RVExtension(char* output, int outputSize, const char* function) {
  ::RVExtensionArgs(output, outputSize, function, nullptr, 0);
}

int RVExtensionArgs(char* output, int outputSize, const char* function, const char* argv[], int argc) {
  try {
    std::string command(function);
    std::vector<std::string> args;
    std::stringstream commandOutput;

    SPDLOG_DEBUG("Received command \"{}\" with args:", command);

    for (int i = 0; i < argc; i++) {
      args.push_back(std::string(argv[i]));

      SPDLOG_DEBUG("{} - {}", i, argv[i]);
    }

    TCS_BattleTracker::BattleTracker::getInstance()->executeCommand(command, args, commandOutput);

    auto stringOutput = commandOutput.str();
    if (stringOutput.size() > 0) {
      ::memcpy(output, stringOutput.c_str(), std::min(stringOutput.size(), static_cast<size_t>(outputSize)));
    }

  } catch (std::exception& e) {
    SPDLOG_CRITICAL("Error during execution of command \"{}\"", function);

    std::cout << e.what() << std::endl;
    SPDLOG_CRITICAL( "Exception: {}", e.what());

    SPDLOG_CRITICAL("Arguments:");
    for (int i = 0; i < argc; i++) {
      SPDLOG_CRITICAL("Arg-{}: {}", i, argv[i]);
    }

  }
  return 0;
}