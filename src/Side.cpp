#include <Side.h>

TCS_BattleTracker::Side TCS_BattleTracker::sideFromString(const std::string& sideString) noexcept {
  if (sideString == "WEST") {
    return TCS_BattleTracker::Side::WEST;
  }

  if (sideString == "EAST") {
    return TCS_BattleTracker::Side::EAST;
  }

  if (sideString == "GUER") {
    return TCS_BattleTracker::Side::INDEPENDENT;
  }

  if (sideString == "CIV") {
    return TCS_BattleTracker::Side::CIVILIAN;
  }
  return TCS_BattleTracker::Side::UNKNOWN;
}
