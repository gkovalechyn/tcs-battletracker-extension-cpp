#pragma once
#include <Transform.h>
#include <VehicleStatus.h>
namespace TCS_BattleTracker {
  struct Snapshot : Serializable {
    float time = 0;
    VehicleStatus status = VehicleStatus::DEFAULT;
    Transform transform = Transform();

    // Inherited via Serializable
    virtual void serialize(BinaryOutputStream& output) const override {
      output.write(this->time);
      output.write(this->status);

      this->transform.serialize(output);
    };
  };
}