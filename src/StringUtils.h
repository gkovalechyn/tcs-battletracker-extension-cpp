#pragma once

#include <algorithm> 
#include <cctype>
#include <locale>
#include <vector>
#include <string>
//Because fuck having a decent string library right?

// trim from start (in place)
namespace StringUtils {
	static inline void trimLeft(std::string& s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
			return !std::isspace(ch);
			})
		);
	}

	// trim from end (in place)
	static inline void trimRight(std::string& s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
			return !std::isspace(ch);
			}).base(), s.end());
	}

	// trim from both ends (in place)
	static inline void trim(std::string& s) {
		trimLeft(s);
		trimRight(s);
	}

	// trim from start (copying)
	static inline std::string trimLeftCopy(std::string s) {
		trimLeft(s);
		return s;
	}

	// trim from end (copying)
	static inline std::string trimRightCopy(std::string s) {
		trimRight(s);
		return s;
	}

	// trim from both ends (copying)
	static inline std::string trimCopy(std::string s) {
		trim(s);
		return s;
	}

	static inline std::vector<std::string> split(std::string string, std::string delimiter) {
		size_t last = 0;
		size_t next = 0;
		std::vector<std::string> parts;

		while ((next = string.find(delimiter, last)) != std::string::npos) {
			parts.push_back(string.substr(last, next - last));
			last = next + 1; 
		}

		parts.push_back(string.substr(last));

		return std::move(parts);
	}

  static inline bool startsWith(const std::string& string, const std::string& start) {
    return string.rfind(start, 0) == 0;
  }

  static inline bool endsWith(std::string const& string, std::string const& end) {
    if (string.length() >= end.length()) {
      return string.compare(string.length() - end.length(), end.length(), end) == 0;
    } else {
      return false;
    }
  }

}