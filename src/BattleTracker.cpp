#include <BattleTracker.h>
#include <Commands/MissionStarted.h>
#include <Commands/MissionEnded.h>
#include <Commands/UpdateVehicle.h>
#include <Commands/EntityDeleted.h>
#include <Commands/EntityKilled.h>

#include <Commands/VehicleFired.h>
#include <Commands/VehicleHit.h>
#include <Commands/VehicleInit.h>

#include <Commands/Info.h>
#include <Commands/ReloadConfiguration.h>

#include <Version.h>

#include <sstream>
#include <iomanip>
#include <thread>
#include <fstream>
#include <chrono>

std::unique_ptr<TCS_BattleTracker::BattleTracker> TCS_BattleTracker::BattleTracker::instance;

TCS_BattleTracker::BattleTracker::BattleTracker() {
  this->reloadConfiguration();
  this->registerCommands();
}

void TCS_BattleTracker::BattleTracker::registerCommands() {
  this->commands["missionStarted"] = std::make_unique<MissionStartedCommand>();
  this->commands["missionEnded"] = std::make_unique<MissionEndedCommand>();
  this->commands["updateVehicle"] = std::make_unique<UpdateVehicleCommand>();

  this->commands["entityDeleted"] = std::make_unique<EntityDeletedCommand>();
  this->commands["entityKilled"] = std::make_unique<EntityKilledCommand>();

  this->commands["vehicleFired"] = std::make_unique<VehicleFiredCommand>();
  this->commands["vehicleHit"] = std::make_unique<VehicleHitCommand>();
  this->commands["vehicleInit"] = std::make_unique<VehicleInitCommand>();

  this->commands["info"] = std::make_unique<InfoCommand>();
  this->commands["reloadConfiguration"] = std::make_unique<ReloadConfigurationCommand>();
}

void TCS_BattleTracker::BattleTracker::initialize() {
  instance = std::make_unique<BattleTracker>();
}

TCS_BattleTracker::BattleTracker* TCS_BattleTracker::BattleTracker::getInstance() {
  return BattleTracker::instance.get();
}

void TCS_BattleTracker::BattleTracker::reloadConfiguration() {
  this->config.loadFrom("BattleTracker.cfg");
  this->config.saveTo("BattleTracker.cfg");
}

TCS_BattleTracker::Vehicle* TCS_BattleTracker::BattleTracker::getVehicle(ID id) {
  auto it = this->vehicles.find(id);

  if (it != this->vehicles.end()) {
    return &it->second;
  } else {
    return nullptr;
  }
}

void TCS_BattleTracker::BattleTracker::registerVehicle(const Vehicle& vehicle) {
  if (this->vehicles.find(vehicle.id) != this->vehicles.end()) {
    SPDLOG_ERROR("Tried to register a vehicle with ID {} twice", vehicle.id);
    return;
  }

  this->vehicles[vehicle.id] = vehicle;
}

void TCS_BattleTracker::BattleTracker::executeCommand(const std::string& command, const std::vector<std::string>& args, std::stringstream& output) {
  auto it = this->commands.find(command);

  if (it == this->commands.end()) {
    SPDLOG_ERROR("Tried to execute a command that did not exist: {}", command);
    return;
  }

  it->second->execute(args, output);
}

void TCS_BattleTracker::BattleTracker::missionStarted(const MissionInfo& info) {
  if (this->isRecording) {
    SPDLOG_ERROR("Tried to start a mission while there is already a recording in progress");
    SPDLOG_ERROR("Cleaning up and starting again");
    this->vehicles.clear();
  }

  this->missionInfo = info;
  this->isRecording = true;
}

void TCS_BattleTracker::BattleTracker::missionEnded(float time) {
  if (!this->isRecording) {
    SPDLOG_ERROR("Tried to end a recording while there is none running");
    return;
  }

  this->missionInfo.endTime = time;

  std::string path = config.get("exportPath");

  this->beginExport(path);
  this->isRecording = false;
}

void TCS_BattleTracker::BattleTracker::beginExport(std::string folder) {
  // Need to disable this warning otherwise it fails to compile because it thinks localtime is unsafe on MSVC
#ifdef _MSC_VER
  #pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS
#endif
  auto time = std::time(nullptr);
  auto now = std::localtime(&time);

  std::stringstream stream;
  stream << std::put_time(now, "[%F]");
  stream << missionInfo.worldName << "-" << missionInfo.missionName << ".btr";

  std::string fullPath = folder + stream.str();

  SPDLOG_INFO("Exporting mission file: {}", fullPath);
  spdlog::default_logger()->flush();

  auto thread = std::thread(&BattleTracker::exportFunction, fullPath, this->missionInfo, this->vehicles);
  thread.detach();

  // Everything should be copied already, so we're safe to clear everything
  // Mission info doesn't matter, only the vehicles do
  this->vehicles.clear();
}

void TCS_BattleTracker::BattleTracker::exportFunction(std::string filename, MissionInfo missionInfo, std::unordered_map<ID, Vehicle> vehicles) {
  try {
    std::ofstream file(filename, std::ios::binary | std::ios::out);

    if (!file.is_open()) {
      SPDLOG_CRITICAL("Failed to create file \"{}\", does arma have permissions to access that path?", filename);
      return;
    }

    auto binaryStream = std::make_unique<BinaryOutputStream>(&file);
    auto start = std::chrono::high_resolution_clock::now();

    SPDLOG_INFO("Starting export to {}", filename);

    // Header
    binaryStream->write((char)0x42); // B
    binaryStream->write((char)0x54); // T

    // Recording file version
    binaryStream->write(1);
    binaryStream->write(0);
    binaryStream->write(0);

    missionInfo.serialize(*binaryStream);

    binaryStream->write(static_cast<int32_t>(vehicles.size()));

    for (auto entry : vehicles) {
      entry.second.serialize(*binaryStream);
    }

    SPDLOG_INFO("Finishing export");
    file.flush();
    file.close();

    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    SPDLOG_INFO("Export took {}ms", elapsed.count());
    spdlog::default_logger()->flush();
  } catch (const std::exception& e) {
    SPDLOG_CRITICAL("Failed to export recording, exception: {}", e.what());
    spdlog::default_logger()->flush();
  }
}
