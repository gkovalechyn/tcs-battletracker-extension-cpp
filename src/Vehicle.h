#pragma once
#include <string>

#include <VehicleType.h>
#include <Side.h>
#include <Events/FiredEvent.h>
#include <Events/Event.h>
#include <Timeline.h>
#include <VehicleStatus.h>

typedef int32_t ID;

namespace TCS_BattleTracker {
  struct Vehicle : Serializable{
    ID id = 0;
    VehicleType type = VehicleType::MAN;
    std::string name = "";
    bool isPlayer = false;
    Side side = Side::UNKNOWN;
    float createdTime = 0;
    float deletedTime = 0;
    Timeline timeline;

    // Inherited via Serializable
    virtual void serialize(BinaryOutputStream& output) const override {
      output.write(this->id);
      output.write(this->type);

      // Assuming strings are UTF-8 encoded
      output.write(static_cast<int32_t>(this->name.length()));
      output.write(this->name.c_str(), this->name.length());

      output.write(this->side);
      output.write(static_cast<char>(this->isPlayer));
      output.write(this->createdTime);
      output.write(this->deletedTime);

      this->timeline.serialize(output);
    }
  };
}