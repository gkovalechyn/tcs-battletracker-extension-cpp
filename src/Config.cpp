#include "Config.h"
#include <spdlog/spdlog.h>
#include <fstream>

#include "StringUtils.h"

TCS_BattleTracker::Config::Config() {
  // Setup default keys
  // this->config["exportPath"] = std::filesystem::current_path().string() + "/";
  this->config["exportPath"] = "./"; // Export to the current working directory
}

void TCS_BattleTracker::Config::loadFrom(const std::string& path) {
  std::fstream file(path, std::ios::in);

  if (!file.is_open()) {
    SPDLOG_CRITICAL("Failed to open configuration file {}", path);
    return;
  }

  std::string line;
  while (std::getline(file, line)) {
    StringUtils::trim(line);

    if (StringUtils::startsWith(line, "#")) {
      continue;
    }

    auto parts = StringUtils::split(line, "=");
    if (parts.size() < 2) {
      SPDLOG_INFO("Ignored configuration line \"{}\" because it has no value", line);
      continue;
    }

    this->config[parts.at(0)] = parts.at(1);
  }
}

void TCS_BattleTracker::Config::saveTo(const std::string& path) {
  std::fstream out(path, std::ios::out | std::ios::trunc);

  for (auto entry : this->config) {
    out << entry.first << "=" << entry.second << std::endl;
  }
}

std::string TCS_BattleTracker::Config::get(const std::string& key) {
  if (this->config.find(key) == this->config.end()) {
    return "";
  } else {
    return this->config.at(key);
  }
}
