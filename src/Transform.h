#pragma once
#include <Vector3.h>

namespace TCS_BattleTracker {
  struct Transform : Serializable{
    Vector3 position;
    float rotation = 0;

    virtual void serialize(BinaryOutputStream& output) const override {
      this->position.serialize(output);
      output.write(this->rotation);
    }
  };
}