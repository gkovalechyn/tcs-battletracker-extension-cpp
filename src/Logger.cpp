#include <Logger.h>

void TCS_BattleTracker::initializeLogger() {
  TCS_BattleTracker::FILE_LOGGER = spdlog::basic_logger_mt("fileLogger", "./TCS_BattleTracker.log", true);
  TCS_BattleTracker::FILE_LOGGER->set_level(spdlog::level::debug);

  spdlog::set_default_logger(TCS_BattleTracker::FILE_LOGGER);
}