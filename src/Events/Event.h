#pragma once
#include <fstream>
#include <Serializable.h>

namespace TCS_BattleTracker {
  class Event: public Serializable {
  private:
    float timestamp = 0;

  public:
    Event(float timestamp) : timestamp(timestamp) {
    }
    virtual ~Event() {}

    float getTimestamp() const {
      return this->timestamp;
    }
  };
}