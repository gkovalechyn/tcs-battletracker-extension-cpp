#pragma once
#include <Events/Event.h>
#include <Vector3.h>

namespace TCS_BattleTracker {
  class FiredEvent : public Event {
  private:
    Vector3 firedPosition;
    Vector3 projectileHitPosition;
    float projectileHitTimestamp;

  public:
    FiredEvent(float timestamp, const Vector3& firedPosition, const Vector3& projectileHitPosition, float projectileHitTimestamp) :
      Event(timestamp),
      firedPosition(firedPosition),
      projectileHitPosition(projectileHitPosition),
      projectileHitTimestamp(projectileHitTimestamp) {
    }

    // Inherited via Event
    void serialize(BinaryOutputStream& output) const override {
      const int32_t dataLength = 4 // Hit time
        + (4 * 3)              // My position
        + (4 * 3)              // Fired position
        + 4;                   // Fired time

      // TLV structure
      output.write(3); // Hit event ID
      output.write(dataLength);

      output.write(this->getTimestamp());

      this->firedPosition.serialize(output);
      this->projectileHitPosition.serialize(output);

      output.write(this->projectileHitTimestamp);
    }
  };
}