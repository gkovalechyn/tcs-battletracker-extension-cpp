#pragma once
#include <Events/Event.h>
#include <Vector3.h>

namespace TCS_BattleTracker {
  class HitEvent : public Event {
  private:
    ID instigatorID;

    Vector3 instigatorPosition;
    Vector3 myPosition;
  public:
    HitEvent(float timestamp, ID instigatorID, const Vector3& instigatorPosition, const Vector3& myPosition) :
      Event(timestamp),
      instigatorID(instigatorID),
      instigatorPosition(instigatorPosition),
      myPosition(myPosition) {}

    // Inherited via Event
    void serialize(BinaryOutputStream& output) const override {
      const int32_t dataLength = 4 // Hit time
        + (4 * 3)              // My position
        + (4 * 3)              // Instigator position
        + 4;                   // Instigator ID

      // TLV structure

      output.write(4); // Hit event ID
      output.write(dataLength);

      output.write(this->getTimestamp());
      this->myPosition.serialize(output);
      this->instigatorPosition.serialize(output);
      output.write(this->instigatorID);
    }
  };
}