#pragma once
#include <Events/Event.h>
#include <Vector3.h>

namespace TCS_BattleTracker {
  class KilledEvent : public Event {
  private:
    Vector3 myPosition;

    ID victimID;
    Vector3 victimPosition;
  public:
    KilledEvent(float timestamp, Vector3 myPosition, ID victimID, Vector3 victimPosition):
      Event(timestamp),
      myPosition(myPosition),
      victimID(victimID),
      victimPosition(victimPosition) {}

    // Inherited via Event
    virtual void serialize(BinaryOutputStream& output) const override {
      const int32_t dataLength = 4 // Hit time
        + (4 * 3)              // My position
        + (4 * 3)              // Victim position
        + 4;                   // Victim ID

      // TLV structure

      output.write(5); // Hit event ID
      output.write(dataLength);

      output.write(this->getTimestamp());
      this->myPosition.serialize(output);
      this->victimPosition.serialize(output);
      output.write(this->victimID);
    }
  };
}