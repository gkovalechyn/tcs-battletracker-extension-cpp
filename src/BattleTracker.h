#pragma once

#include <memory>
#include <unordered_map>
#include <Commands/Command.h>
#include <Vehicle.h>
#include <MissionInfo.h>
#include <Config.h>

namespace TCS_BattleTracker {
  class BattleTracker {

  private:
    static std::unique_ptr<BattleTracker> instance;

    std::unordered_map<std::string, std::unique_ptr<Command>> commands;
    std::unordered_map<ID, Vehicle> vehicles;
    MissionInfo missionInfo;
    bool isRecording = false;
    Config config;

    void registerCommands();
  public:
    BattleTracker();

    static void initialize();
    static BattleTracker* getInstance();
    void reloadConfiguration();

    Vehicle* getVehicle(ID id);
    void registerVehicle(const Vehicle& vehicle);

    void executeCommand(const std::string& command, const std::vector<std::string>& args, std::stringstream& output);

    void missionStarted(const MissionInfo& missionInfo);
    void missionEnded(float time);

    void beginExport(std::string folder);
    
    static void exportFunction(std::string filename, MissionInfo missionInfo, std::unordered_map<ID, Vehicle> vehicles);
  };
}