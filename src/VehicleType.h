#pragma once
#include <cstdint>

namespace TCS_BattleTracker {
  enum VehicleType : int32_t {
    MAN = 0,
    HELICOPTER = 1,
    PLANE = 2,
    CAR = 3,
    TANK = 4,
    SHIP = 5,
    STATIC_WEAPON = 6
  };
}