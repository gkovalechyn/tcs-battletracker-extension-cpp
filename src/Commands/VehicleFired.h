#pragma once
#include <Commands/Command.h>
#include <BattleTracker.h>
#include <Events/FiredEvent.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class VehicleFiredCommand : public Command {
    /// <summary>
    /// Command handler for a when a vehicle fires a weapon and the bullet impacts somewhere.
    /// </summary>
    /// <param name="args">
    ///   0 - (long) Unit ID who just fired
    ///   1 - (vec3) Where the unit fired from
    ///   2 - (float) When the unit fired
    ///   3 - (vec3) Where the projectile hit
    ///   4 - (float) When the projectile hit
    /// </param>
    ///
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);

      if (args.size() != 5) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 5, args.size());
        return;
      }

      auto vehicle = BattleTracker::getInstance()->getVehicle(std::stoi(args.at(0)));

      if (vehicle == nullptr) {
        SPDLOG_ERROR("Received a fired event for vehicle {} that was never tracked", args.at(0));
        return;
      }
      Vector3 firedPosition = Vector3::fromArma3Array(args.at(1));
      float firedTime = std::stof(args.at(2));

      Vector3 hitPosition = Vector3::fromArma3Array(args.at(3));
      float hitTime = std::stof(args.at(4));

      auto evt = std::make_unique<FiredEvent>(firedTime, firedPosition, hitPosition, hitTime);
      vehicle->timeline.recordEvent(std::move(evt));
    }
  };
}