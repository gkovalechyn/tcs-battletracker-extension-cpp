#pragma once
#include <Commands/Command.h>
#include <BattleTracker.h>
#include <Events/HitEvent.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class VehicleHitCommand : public Command {
    /// <summary>
    /// Handles the command where a unit was hit by something.
    /// </summary>
    /// <param name="args">
    ///   0 - (long) Instigator ID
    ///   1 - (vec3) Instigator position
    ///   2 - (long) Hit unit ID
    ///   3 - (vec3) Hit unit Position
    ///   4 - (float) timestamp
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);

      if (args.size() != 5) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 5, args.size());
        return;
      }

      ID instigatorID = std::stoi(args.at(0));
      auto instigator = BattleTracker::getInstance()->getVehicle(instigatorID);

      if (instigator == nullptr) {
        SPDLOG_ERROR("Tried to register a hit with instigator {} that was never tracked", args.at(0));
        return;
      }

      ID victimID = std::stoi(args.at(2));
      auto victim = BattleTracker::getInstance()->getVehicle(victimID);

      if (victim == nullptr) {
        SPDLOG_ERROR("Tried to register a hit with instigator {} that was never tracked", args.at(2));
        return;
      }

      auto instigatorPosition = Vector3::fromArma3Array(args.at(1));
      auto victimPosition = Vector3::fromArma3Array(args.at(3));
      auto time = std::stof(args.at(4));

      auto evt = std::make_unique<HitEvent>(time, instigatorID, instigatorPosition, victimPosition);
      victim->timeline.recordEvent(std::move(evt));
    }
  };
}