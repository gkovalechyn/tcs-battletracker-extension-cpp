#pragma once
#include <Commands/Command.h>
#include <Version.h>
#include <Unused.h>
namespace TCS_BattleTracker {
  class InfoCommand : public Command {
    /// <summary>
    /// Returns an information string about the extension.
    /// </summary>
    /// <param name="args">
    ///   No arguments
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(args);

      output << "TCS-BattleTracker extension v";
      output << VERSION;
      output << "; compiled with: ";

#ifdef WIN32
      output << "MSVC " << _MSC_VER;
#else
      output << "GCC " << __GNUC__;
#endif

      output << " on " << __DATE__;
    }
  };
}