#pragma once
#include <Commands/Command.h>
#include <Version.h>
#include <Unused.h>
#include <spdlog/spdlog.h>
#include <BattleTracker.h>

namespace TCS_BattleTracker {
  class ReloadConfigurationCommand : public Command {
    /// <summary>
    /// Reloads the configuration file.
    /// </summary>
    /// <param name="args">
    ///   No arguments
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(args);

      BattleTracker::getInstance()->reloadConfiguration();

      SPDLOG_INFO("Reloaded configuration file");

      output << "Configuration reloaded";
    }
  };
}