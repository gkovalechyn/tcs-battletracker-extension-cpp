#pragma once
#include <vector>
#include <string>
#include <sstream>

class Command {
public:
	virtual void execute(const std::vector<std::string>& args, std::stringstream& output) = 0;
};