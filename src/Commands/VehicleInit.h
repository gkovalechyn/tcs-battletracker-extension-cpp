#pragma once
#include <Commands/Command.h>
#include <BattleTracker.h>
#include <StringUtils.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class VehicleInitCommand : public Command {
    /// <summary>
    /// Called when a vehicle was spawned.
    ///
    /// @TODO This does not handle units spawning inside a vehicle
    /// </summary>
    ///
    /// <param name="args">
    ///   0 - (int) The unit ID
    ///   1 - (int) The vehicle type <see cref="VehicleType"/>
    ///   2 - (string) The vehicle name
    ///   3 - (Vec3) The initial position
    ///   4 - (float) The initial rotation
    ///   5 - (string) The object's side
    ///   6 - (boolean) Is the vehicle a player
    ///   7 - (float) Timestamp
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);

      if (args.size() != 8) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 8, args.size());
        return;
      }

      ID id = std::stoi(args.at(0));

      if (BattleTracker::getInstance()->getVehicle(id) != nullptr) {
        SPDLOG_ERROR("Tried to register a vehicle with id {} twice", id);
        return;
      }

      auto initialPosition = Vector3::fromArma3Array(args.at(3));
      float initialRotation = std::stof(args.at(4));
      float time = std::stof(args.at(7));
      auto nameCopy = StringUtils::trimCopy(args.at(2));

      Vehicle vehicle;
      vehicle.id = id;
      vehicle.type = (VehicleType)std::stoi(args.at(1));
      vehicle.name = nameCopy.substr(1, nameCopy.length() - 2);
      vehicle.side = sideFromString(args.at(5));
      vehicle.isPlayer = (args.at(6) == "true");
      vehicle.createdTime = time;

      Transform transform;
      transform.position = initialPosition;
      transform.rotation = initialRotation;

      vehicle.timeline.recordSnapshot(time, transform, VehicleStatus::DEFAULT);

      BattleTracker::getInstance()->registerVehicle(vehicle);
    }
  };
}