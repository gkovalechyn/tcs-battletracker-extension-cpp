#pragma once
#include <Commands/Command.h>
#include <BattleTracker.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class EntityDeletedCommand : public Command {
    /// <summary>
    /// Command called when a vehicle is deleted
    /// </summary>
    ///
    /// <param name="args">
    ///   0 - (long) The vehicle ID that was just destroyed
    ///   1 - (float) Timestamp
    /// </param>

    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);
      if (args.size() != 2) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 2, args.size());
        return;
      }

      ID id = std::stoi(args.at(0));
      auto vehicle = BattleTracker::getInstance()->getVehicle(id);

      if (vehicle == nullptr) {
        SPDLOG_ERROR("Tried to mark vehicle {} as deleted but it was never registered", id);
        return;
      }

      float deletedTime;

      try {
        deletedTime = std::stof(args.at(1));
      } catch (const std::invalid_argument&) {
        SPDLOG_ERROR("Invalid number format vector {}", args.at(1));
        return;
      }

      vehicle->deletedTime = deletedTime;
    }
  };
}