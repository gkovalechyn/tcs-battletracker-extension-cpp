#pragma once
#include <Commands/Command.h>
#include <MissionInfo.h>
#include <StringUtils.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class MissionStartedCommand : public Command {
  public:

    // Inherited via Command
    /// <summary>
    /// Handles the Mission start command
    /// </summary>
    /// <param name="args">
    ///   0 - (string) The world name
    ///   1 - (vec2) World size
    ///   2 - (string) The mission name
    ///   3 - (number) Timestamp
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);

      if (args.size() != 4) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 4, args.size());
        return;
      }

      MissionInfo info;
      auto worldNameCopy = StringUtils::trimCopy(args.at(0));
      auto missionNameCopy = StringUtils::trimCopy(args.at(2));
      // Remove both quotes
      info.worldName = worldNameCopy.substr(1, worldNameCopy.length() - 2);
      info.worldSize = Vector3::fromArma3Array(args.at(1));
      info.missionName = missionNameCopy.substr(1, missionNameCopy.length() - 2);
      info.startTime= std::stof(args.at(3));
      
      BattleTracker::getInstance()->missionStarted(info);
    };
  };
}