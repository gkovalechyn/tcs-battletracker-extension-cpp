#pragma once
#include <Commands/Command.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class MissionEndedCommand : public Command {
  public:

    // Inherited via Command
    /// <summary>
    /// Handles the mission end command
    /// </summary>
    /// <param name="args">
    ///   0 - (float) Timestamp of the mission end
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);

      if (args.size() != 1) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 1, args.size());
        return;
      }

      BattleTracker::getInstance()->missionEnded(std::stof(args.at(0)));
    };
  };
}