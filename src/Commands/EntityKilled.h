#pragma once
#include <Commands/Command.h>
#include <BattleTracker.h>
#include <Events/KilledEvent.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class EntityKilledCommand : public Command {
    /// <summary>
    /// Handles the unitKilled command
    /// </summary>
    /// <param name="args">
    ///   0 - (ind) Killer ID
    ///   1 - (vec3) Killer position
    ///   2 - (long) Victim ID
    ///   3 - (vec3) Victim Pos
    ///   4 - (float) timestamp
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);
      if (args.size() != 5) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 5, args.size());
        return;
      }

      ID id = std::stoi(args.at(0));
      auto vehicle = BattleTracker::getInstance()->getVehicle(id);

      if (vehicle == nullptr) {
        SPDLOG_ERROR("Tried to record a kill for vehicle {} but it was never tracked", id);
        return;
      }

      ID victimID = std::stoi(args.at(2));
      if (BattleTracker::getInstance()->getVehicle(victimID) == nullptr) {
        SPDLOG_ERROR("Tried to record a kill for vehicle {} but the victim {} was never tracked", victimID);
        return;
      }

      Vector3 killerPosition = Vector3::fromArma3Array(args.at(1));
      Vector3 victimPosition = Vector3::fromArma3Array(args.at(3));
      float time = std::stof(args.at(4));
      auto evt = std::make_unique<KilledEvent>(time, killerPosition, victimID, victimPosition);

      vehicle->timeline.recordEvent(std::move(evt));
    }
  };
}