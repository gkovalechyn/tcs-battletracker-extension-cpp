#pragma once
#include <Commands/Command.h>
#include <Vehicle.h>
#include <Unused.h>

namespace TCS_BattleTracker {
  class UpdateVehicleCommand : public Command {
  public:

    // Inherited via Command
    /// <summary>
    /// Called when a vehicle needs to be updated
    /// </summary>
    ///
    /// <param name="args">
    ///   0 - (int) The vehicle ID
    ///   1 - (int) Vehicle status
    ///   2 - (Vec3) The new position
    ///   3 - (float) The new rotation
    ///   4 - (float) Timestamp
    /// </param>
    void execute(const std::vector<std::string>& args, std::stringstream& output) {
      UNUSED(output);

      if (args.size() != 5) {
        SPDLOG_ERROR("Unexpected amount of arguments, expected {} but received {}", 5, args.size());
        return;
      }

      ID id = std::stoi(args.at(0));

      auto vehicle = BattleTracker::getInstance()->getVehicle(id);

      if (vehicle == nullptr) {
        SPDLOG_ERROR("Tried to update a vehicle with id {} that was not tracked", id);
        return;
      }

      VehicleStatus newStatus = static_cast<VehicleStatus>(std::stoi(args.at(1)));
      Transform transform;
      transform.position = Vector3::fromArma3Array(args.at(2));
      transform.rotation = std::stof(args.at(3));
      float time = std::stof(args.at(4));


      vehicle->timeline.recordSnapshot(time, transform, newStatus);
    }
  };
}