// All char* are assumed to be UTF-8 encoded strings

extern "C" {
#ifdef WIN32
	// Called by Engine on extension load 
	__declspec (dllexport) void __stdcall RVExtensionVersion(char* output, int outputSize);

	// STRING callExtension STRING
	__declspec (dllexport) void __stdcall RVExtension(char* output, int outputSize, const char* function);

	// STRING callExtension ARRAY
	__declspec (dllexport) int __stdcall RVExtensionArgs(char* output, int outputSize, const char* function, const char* argv[], int argc);
#else
	// Called by Engine on extension load 
  __attribute__((visibility("default"))) void RVExtensionVersion(char* output, int outputSize);

	// STRING callExtension STRING
  __attribute__((visibility("default"))) void RVExtension(char* output, int outputSize, const char* function);

	// STRING callExtension ARRAY
  __attribute__((visibility("default"))) int RVExtensionArgs(char* output, int outputSize, const char* function, const char* argv[], int argc);
#endif
}