#pragma once
#include <string>
#include <Vector3.h>

namespace TCS_BattleTracker {
  struct MissionInfo : Serializable{
    std::string worldName;
    Vector3 worldSize;

    std::string missionName;
    float startTime;
    float endTime;

    // Inherited via Serializable
    virtual void serialize(BinaryOutputStream& output) const override {
      output.write(static_cast<int32_t>(worldName.length()));
      output.write(worldName.c_str(), worldName.length());

      output.write(static_cast<int32_t>(missionName.length()));
      output.write(missionName.c_str(), missionName.length());

      this->worldSize.serialize(output);

      output.write(startTime);
      output.write(endTime);
    }
  };
}