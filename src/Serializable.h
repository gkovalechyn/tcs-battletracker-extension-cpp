#pragma once
#include <BinaryOutputStream.h>

namespace TCS_BattleTracker {
  class Serializable {
  public:
    virtual ~Serializable() {}
    virtual void serialize(BinaryOutputStream& output) const = 0;
  };
}