meta:
  id: recording
  file-extension: btr
  endian: le
seq:
  - id: header
    type: header
    
  - id: mission_data
    type: mission_data
    
  - id: vehicle_count
    type: s4
  - id: vehicles
    type: vehicle
    repeat: expr
    repeat-expr: vehicle_count
    

enums:
  vehicle_side:
    0: "west"
    1: "east"
    2: "independent"
    3: "civilian"
    4: "unknown"
    
  vehicle_type:
    0: "man"
    1: "helicopter"
    2: "plane"
    3: "car"
    4: "tank"
    5: "ship"
    6: "static_weapon"
    
  vehicle_status:
    0: "default"
    1: "in_vehicle"
    2: "unconscious"
    3: "dead"
  
  event_type:
    # Events 0-2 are no longer used because
    # they have been replaced by the Status flag
    # 0: "created"
    # 1: "deleted"
    # 2: "died"
    3: "fired"
    4: "hit"
    5: "kill"
    
types: 
  string_utf8:
    seq:
		# This is the size in bytes of the string, not the number of characters
      - id: size
        type: s4
      - id: data
        type: str
        size: size
        encoding: "UTF-8"
        
  header:
    seq:
      - id: magic
        contents: BT
      - id: version_major
        type: s4
        contents: [0x01, 0x00, 0x00, 0x00]
      - id: version_minor
        type: s4
        contents: [0x00, 0x00, 0x00, 0x00]
      - id: version_patch
        type: s4
        contents: [0x00, 0x00, 0x00, 0x00]
        
  mission_data:
    seq:
      - id: world
        type: string_utf8
      - id: mission
        type: string_utf8

      - id: world_size
        type: vector3

      - id: start_time
        type: f4
      - id: end_time
        type: f4

  vector3:
    seq:
      - id: x
        type: f4
      - id: y
        type: f4
      - id: z
        type: f4
        
  vehicle:
    seq:
      - id: id
        type: s4
      - id: type
        type: s4
        enum: vehicle_type
        
      - id: name
        type: string_utf8
      
      - id: side
        type: s4
        enum: vehicle_side
      
      - id: is_player
        type: s1

      - id: created_at
        type: f4
      - id: deleted_at
        type: f4
        
      - id: timeline
        type: timeline
        
  timeline:
    seq:
    - id: snapshot_count
      type: s4
    - id: snapshots
      type: snapshot
      repeat: expr
      repeat-expr: snapshot_count
      
    - id: event_count
      type: s4
    - id: events
      type: event
      repeat: expr
      repeat-expr: event_count
     
  snapshot:
    seq:
      - id: timestamp
        type: f4
      - id: status
        type: s4
        enum: vehicle_status
      - id: transform
        type: transform
  
  transform:
    seq: 
      - id: position
        type: vector3
      - id: rotation
        type: f4
        
  event:
    seq:
      - id: event_type
        type: s4
        # For some reason when this is enabled kaitai wants the event_type to be a u1
        # and doesn't care that the type is s4. 
        # enum: event_type
      - id: length
        type: s4
      - id: body
        size: length
        type:
          switch-on: event_type
          cases:
            3: fired_event
            4: hit_event
            5: kill_event
            
  simple_event:
    seq:
      - id: timestamp
        type: f4

  fired_event:
    seq:
      - id: timestamp
        type: f4
      - id: fired_position
        type: vector3
      - id: projectile_hit_position
        type: vector3
      - id: projectile_hit_timestamp
        type: f4
        
  hit_event:
    seq:
      - id: timestamp
        type: f4
      - id: my_position
        type: vector3
      - id: instigator_position
        type: vector3
      - id: instigator_id
        type: s4

  kill_event:
    seq:
      - id: timestamp
        type: f4
      - id: my_position
        type: vector3
      - id: victim_position
        type: vector3
      - id: victim_id
        type: s4
      